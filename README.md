# Building/Running/Testing
## Run it 
 ./mvnw spring-boot:run
 
## Package and run it.
1. ./mvnw package
2. java -jar target/drone-0.0.1-SNAPSHOT.jar

# Accessing the H2 in-memory database
1. Navigate to localhost:8080/h2-console with the web browser. 
2. Use jdbc:h2:mem:testdb or jdbc:h2:mem:testdb as the JDBC URL and leave the Password field empty
