package com.gregomebije.drone;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.gregomebije.drone.model.Drone;
import com.gregomebije.drone.model.DroneModel;
import com.gregomebije.drone.model.DroneState;
import com.gregomebije.drone.repository.DroneRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DroneRepositoryTest {
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private DroneRepository repository;
	
	@Test
	public void saveDone() {
		Drone drone = new Drone("DroneTest", DroneModel.Lightweight, 50, 50, DroneState.IDLE);
		entityManager.persistAndFlush(drone);
		assertThat(drone.getId()).isNotNull();
	}
	
	@Test
	public void deleteCars() {
		entityManager.persistAndFlush(new Drone("AB12333", DroneModel.Middleweight, 50, 50, DroneState.IDLE));
		entityManager.persistAndFlush(new Drone("XYZ98788", DroneModel.Lightweight, 50, 50, DroneState.IDLE));
       
		repository.deleteAll();
		assertThat(repository.findAll()).isEmpty();
	} 
	
}
