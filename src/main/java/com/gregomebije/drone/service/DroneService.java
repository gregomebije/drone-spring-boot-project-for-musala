package com.gregomebije.drone.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gregomebije.drone.model.Drone;
import com.gregomebije.drone.model.DroneModel;
import com.gregomebije.drone.model.DroneRequestDto;
import com.gregomebije.drone.model.DroneResponseDto;
import com.gregomebije.drone.model.DroneState;
import com.gregomebije.drone.repository.DroneRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor

public class DroneService {
	
    @Autowired
    DroneRepository droneRepository;
    
    
    public DroneResponseDto register(DroneRequestDto request) {
    	
    	System.out.println(request);
    	
    	Drone drone = new Drone();
    	drone.setSerialNumber(request.getSerialNumber());
    	drone.setDroneModel(request.getModel());
    	drone.setWeight(request.getWeight());
    	drone.setBatteryCapacity(50);
    	drone.setDroneState(DroneState.IDLE);
        
        DroneResponseDto response = new DroneResponseDto();
        response.setStatus(true);
        response.setMessage("transaction request successful");
        response.setData(drone);
       
        return response;
    }
    
}
