package com.gregomebije.drone.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Drone {
	 @Id
     @GeneratedValue(strategy=GenerationType.AUTO)
     private long id;
	 
	 @Column(length=100)
     private String serialNumber;
     
     private DroneModel droneModel;
     
     @Column(length=500)
     private int weight;
     
     private int batteryCapacity;
     private DroneState droneState;
 
     @OneToMany(mappedBy = "drone")
     Set<DroneMedication> droneMedications;
     
     @Column(name = "date_created", nullable = false, insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
     @Temporal(TemporalType.TIMESTAMP)
     private Date dateCreated;

     @JsonIgnore
     @Column(name = "date_updated", nullable = false, insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
     @Temporal(TemporalType.TIMESTAMP)
     private Date dateUpdated;
     
     public Drone() {
    	 
     }
     
     public Drone(String serialNumber, DroneModel droneModel, int weight, int batteryCapacity, DroneState droneState) {
    	 this.serialNumber = serialNumber;
    	 this.droneModel = droneModel;
    	 this.weight  = weight;
    	 this.batteryCapacity = batteryCapacity;
    	 this.droneState = droneState;
     }
     
     public Long getId() {
     	return this.id;
     }
     
     public String getSerialNumber() {
    	 return this.serialNumber;
     }
     public void setSerialNumber(String serialNumber) {
    	 this.serialNumber = serialNumber;
     }
     
     public DroneModel getDroneModel() {
    	 return this.droneModel;
     }
     public void setDroneModel(DroneModel droneModel) {
    	 this.droneModel = droneModel;
     }
     
     public int getWeight() {
    	 return this.weight;
     }
     public void setWeight(int weight) {
    	 this.weight = weight;
     }
     
     public int getBatteryCapacity() {
    	 return this.batteryCapacity;
     }
     public void setBatteryCapacity(int batteryCapacity) {
    	 this.batteryCapacity = batteryCapacity;
     }
     
     public DroneState getDroneState() {
    	 return this.droneState;
     }
     public void setDroneState(DroneState droneState) {
    	 this.droneState = droneState;
     }
     
       
}