package com.gregomebije.drone.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Medication {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
	 
	private String name; //(allowed only letters, numbers, ‘-‘, ‘_’);
	private int weight;
	private String code; //(allowed only upper case letters, underscore and numbers);
	private String image; //(picture of the medication case).
	
	
	@OneToMany(mappedBy = "medication")
    Set<DroneMedication> droneMedications;
	
	  
	@Column(name = "date_created", nullable = false, insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;

    @JsonIgnore
    @Column(name = "date_updated", nullable = false, insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdated;

    public Medication(String name, int weight, String code, String image) {
    	this.name = name;
    	this.weight = weight;
    	this.code = code;
    	this.image = image;
    }
    
    public Long getId() {
    	return this.id;
    }
    
    public String getName() {
    	return this.name;
    }
    public void setName(String name) {
    	this.name = name;
    }
    
    public int getWeight() {
    	return this.weight;
    }
    public void setWeight(int weight) {
    	this.weight = weight;
    }
    
    public String getCode() {
    	return this.code;
    }
    
    public void setCode(String code) {
    	this.code = code;
    }
    
    public String getImage() {
    	return this.image;
    }
    public void setImage(String image) {
    	this.image = image;
    }
      
}
