package com.gregomebije.drone.model;

public enum DroneState {
	IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING;
}