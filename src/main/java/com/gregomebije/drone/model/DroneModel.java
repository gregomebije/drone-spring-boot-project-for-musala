package com.gregomebije.drone.model;

public enum DroneModel {
	Lightweight, Middleweight, Cruiserweight, Heavyweight;
}