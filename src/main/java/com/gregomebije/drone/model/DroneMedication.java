package com.gregomebije.drone.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class DroneMedication {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
	
	@ManyToOne
    @JoinColumn(name = "drone_id")
    Drone drone;

    @ManyToOne
    @JoinColumn(name = "medication_id")
    Medication medication;
    
    public DroneMedication(Drone drone, Medication medication) {
    	this.drone = drone;
    	this.medication = medication;
    }

}
