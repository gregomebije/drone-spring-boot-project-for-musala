package com.gregomebije.drone.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DroneRequestDto {
    private String serialNumber;
    private DroneModel model;
    private int weight;
}