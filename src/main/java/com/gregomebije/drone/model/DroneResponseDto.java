package com.gregomebije.drone.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DroneResponseDto {
    private boolean status;
    private String message;
    private Drone data;
}