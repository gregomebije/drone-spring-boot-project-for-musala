package com.gregomebije.drone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.gregomebije.drone.model.Drone;
import com.gregomebije.drone.model.DroneMedication;
import com.gregomebije.drone.model.DroneModel;
import com.gregomebije.drone.model.DroneState;
import com.gregomebije.drone.model.Medication;
import com.gregomebije.drone.repository.DroneMedicationRepository;
import com.gregomebije.drone.repository.DroneRepository;
import com.gregomebije.drone.repository.MedicationRepository;

@SpringBootApplication
public class DroneApplication {
	
	@Autowired
	DroneRepository droneRepository;
	
	@Autowired
	MedicationRepository medicationRepository;
	
	@Autowired
	DroneMedicationRepository droneMedicationRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(DroneApplication.class);
	
	
	public static void main(String[] args) {
		SpringApplication.run(DroneApplication.class, args);
		logger.info("Hello Spring Boot");
	}
	
	/*
	@Bean
    CommandLineRunner runner(){
      return args -> {
        
        Drone drone1 = new Drone("Ford", DroneModel.Lightweight, 50, 50, DroneState.IDLE);
        droneRepository.save(drone1);
        droneRepository.save(new Drone("AB123", DroneModel.Middleweight, 50, 50, DroneState.IDLE));
        droneRepository.save(new Drone("XYZ987", DroneModel.Lightweight, 50, 50, DroneState.IDLE));
       
        Medication medication1 = new Medication("Medication1", 50, "23ABC", "image1");
        medicationRepository.save(medication1);
        medicationRepository.save(new Medication("Medication2", 40, "23XYZ", "image2"));
        
        DroneMedication dm = new DroneMedication(drone1, medication1);
        droneMedicationRepository.save(dm);
        
      };
    } 
    */
	
}
