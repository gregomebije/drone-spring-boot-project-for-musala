package com.gregomebije.drone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gregomebije.drone.model.DroneRequestDto;
import com.gregomebije.drone.model.DroneResponseDto;
import com.gregomebije.drone.service.DroneService;

@RestController
@RequestMapping(value = "drone", produces = "application/json")
public class DroneController {
	
	@Autowired
	private DroneService droneService;
	
	@PostMapping(value="register")
    public ResponseEntity<DroneResponseDto> register(@RequestBody DroneRequestDto request) {
		return new ResponseEntity<DroneResponseDto>(droneService.register(request), HttpStatus.OK);
        
    }
}
