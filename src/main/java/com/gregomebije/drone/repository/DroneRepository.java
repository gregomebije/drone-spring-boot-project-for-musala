package com.gregomebije.drone.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gregomebije.drone.model.Drone;
import com.gregomebije.drone.model.DroneModel;
import com.gregomebije.drone.model.DroneState;

public interface DroneRepository extends CrudRepository <Drone, Long>{
	
	  List<Drone> findBySerialNumber(String serialNumber);
	  
	  List<Drone> findByDroneModel(DroneModel droneModel);
	  
	  List<Drone> findByWeight(int weight);
	  
	  //checking available drones for loading
	  List<Drone> findByDroneStateOrderByDateCreatedAsc(DroneState droneState);
	   
}
