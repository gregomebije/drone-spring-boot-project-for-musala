package com.gregomebije.drone.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gregomebije.drone.model.Drone;
import com.gregomebije.drone.model.DroneMedication;
import com.gregomebije.drone.model.DroneModel;
import com.gregomebije.drone.model.DroneState;

public interface DroneMedicationRepository extends CrudRepository <DroneMedication, Long>{
	
	   
}
